using System;
using System.IO;

namespace ICTPRG302_Intro_to_Programming {
	class FileReader {

		/// <summary>
		/// Holds the list of usernames in memory, can be accessed by other classes rather than constantly calling LoadUsernames and reading from disk
		/// </summary>
		public List<string> usernamesList = new List<string>();


		// Default file search path
		const string defaultpath = "Gamertags.txt";
		
		/// <summary>
		/// Loads the usernames from a file. Takes an optional file path to search, defaults to Gamertags.txt in the current directory
		/// </summary>
		public bool LoadUsernames(string altpath = "") {
			// create variable to hold name of valid file location
			string namesfile;
			
			if (altpath != "" && File.Exists(altpath)) {
				// If the file passed in argument 1 exists and is valid use that
				namesfile = altpath;
			} else if (File.Exists(defaultpath)) {
				// Otherwise default to Gamertags.txt
				if (altpath != "") {
					// Print warning but continue. To not show this warning remove arguments when calling program
					Console.WriteLine("Invalid file path, defaulting to Gamertags.txt");
				}
				namesfile = defaultpath;
			} else {
				// Throw error when no file is found
				Console.WriteLine("File not found. Please create a file with usernames to read.");
				return false;
			}

			// load file contents to array of names
			StreamReader usernameFile = new StreamReader(namesfile);
			// Holds the contents of the current line
			string? line;
			// Iterate through lines until none left
			while ((line = usernameFile.ReadLine()) != null) {
				usernamesList.Add(line);
			}
			usernameFile.Close();
			// indicate that loading has completed successfully
			return true;
		}

		/// <summary>
		/// Saves usernames to a file, like loading but backwards.
		/// </summary>
		public void SaveUsernames(string altpath = "") {
			// create variable to hold name of valid file location
			string namesfile = GetValidFile(altpath);

			// load file contents to array of names
			StreamWriter usernameFile = new StreamWriter(namesfile);
			foreach (string name in usernamesList) {
				usernameFile.WriteLine(name);
			}
			usernameFile.Close();
		}

		/// <summary>
		/// Returns the file path provided, or the default if it doesn't exist.
		/// </summary>
		public string GetValidFile(string path) {
			if (path != "" && File.Exists(path)) {
				// If the file passed in argument 1 exists and is valid use that
				return path;
			} else if (File.Exists(defaultpath)) {
				// Otherwise default to Gamertags.txt
				if (path != "") {
					// Print warning but continue. To not show this warning remove arguments when calling program
					Console.WriteLine("Invalid file path, defaulting to Gamertags.txt");
				}
				return defaultpath;
			} else {
				// Throw error when no file is found
				Console.WriteLine("File not found.");
				return defaultpath;
			}
		}
	}
}