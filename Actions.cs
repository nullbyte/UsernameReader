namespace ICTPRG302_Intro_to_Programming {
	
	/// <summary>
	/// Base class for all actions to derive from. Specifies methods such as Run to execute the action and PrintHelp to display a help message specific to the particular action
	/// </summary>
	// I could have definitely implemented this better but I'm new to C# so cut me some slack
	class Action {
		protected FileReader fileReader = new FileReader();
		public void LoadFileReader(FileReader reader) {
			fileReader = reader;
		}
		
		/// <summary>
		/// Executes the action.
		/// </summary>
		public virtual void Run(string[] args) {}

		/// <summary>
		/// Prints action specific help text.
		/// </summary>
		public virtual void PrintHelp() {}
	}

	/// <summary>
	/// Action to print the usernames that were loaded
	/// </summary>
	class ListNames: Action {
		public override void Run(string[] args) {
			
			// Check for valid filter mode in arguments, default to all
			FilterMode filter = FilterMode.ALL;
			// Check if there is an argument and it's a number
			if (args.Length > 1 && int.TryParse(args[1], out int _)) {
				int res = int.Parse(args[1]);
				// Check if the number is outside the range 0-5 and default to 0 if it is
				if (res < 0 || res > 5) {
					Console.WriteLine("Filter mode outside range, defaulting to 0");
					res = 0;
				}
				// Apply filter mode
				filter = (FilterMode)res;
			} else if (args.Length > 1) {
				Console.WriteLine("Invalid filter mode, defaulting to 0");
				filter = 0;
			}
			
			// create variable to count line numbers
			int count = 0;

			// Iterate through lines in file and print them with their line number
			foreach (string name in fileReader.usernamesList) {
				if ((filter == FilterMode.ALL) ||
					(filter == FilterMode.NUMBEREND && Char.IsNumber(name[name.Length - 1])) ||
					(filter == FilterMode.UNIQUESTART && !Char.IsLetterOrDigit(name[0])) ||
					(filter == FilterMode.CONTAINSLETTERNUM && name.All(Char.IsLetterOrDigit)) ||
					(filter == FilterMode.UPPERCASE && name.All(Char.IsLetter) && name.All(Char.IsUpper)) ||
					(filter == FilterMode.FIRSTFIVE && count < 5)) {
					count++;
					Console.WriteLine(count.ToString() + ") " + name);
				}
			}
		}

		public override void PrintHelp() {
			Console.WriteLine("Prints all names in the file with the specified filter mode.");
			Console.WriteLine("  Valid filter modes:");
			Console.WriteLine("    0) Show all (default)");
			Console.WriteLine("    1) Show only names ending in a number");
			Console.WriteLine("    2) Show only names starting with neither a number nor a letter");
			Console.WriteLine("    3) Show only names containing only numbers and letters");
			Console.WriteLine("    4) SHOW ONLY NAMES IN ALL CAPS");
			Console.WriteLine("    5) Show only the first five names");
		}
	}

	// An enum for specifying what filter mode to use.
	// Should probably make this more dynamic and less hardcoded... Will do if I have time
	enum FilterMode {
		ALL,
		NUMBEREND,
		UNIQUESTART,
		CONTAINSLETTERNUM,
		UPPERCASE,
		FIRSTFIVE
	}

	/// <summary>
	/// Action to add a username to the end of the list
	/// </summary>
	class AddName: Action {
		public override void Run(string[] args) {
			// Make sure nobody tries to screw with us and add a name spanning two lines
			if (args[1].Contains("\n")) {
				Console.WriteLine("Error: names cannot contain newlines");
				return;
			}
			// Add username and save to a list
			fileReader.usernamesList.Add(args[1]);
			fileReader.SaveUsernames(args.Length > 2 ? args[2] : "");
			Console.WriteLine("Added " + args[1] + " to names list");
			// I can't believe it's that simple, but apparently it is
		}

		public override void PrintHelp()
		{
			Console.WriteLine("Adds name to list of usernames. Optional second argument specifies file path");
			Console.WriteLine("  Example usage:");
			Console.WriteLine("    NamesReader add Alice");
			Console.WriteLine("    NamesReader add Bob names.txt");
		}
	}

	/// <summary>
	/// Action to remove name from the list
	/// </summary>
	class RemoveName: Action {
		public override void Run(string[] args) {			
			// Remove username by index
			if (int.TryParse(args[1], out int _)) {
				int index = int.Parse(args[1]);
				// Check if a username is at the specified line and return if not
				if (fileReader.usernamesList.Count >= index) {
					// Use index + 1 because to the rest of the world counting starts at 1 not 0
					Console.WriteLine("Removing " + fileReader.usernamesList[index - 1] + " from names list");
					fileReader.usernamesList.RemoveAt(index - 1);
				} else {
					Console.WriteLine("No name at specified line");
					return;
				}
			// Remove username by contents
			} else {
				// Check for username and remove if exists
				if (fileReader.usernamesList.Contains(args[1])) {
					fileReader.usernamesList.Remove(args[1]);
					Console.WriteLine("Removed " + args[1] + " from names list");
				} else {
					Console.WriteLine("Username not found in file");
					return;
				}
			}

			// Save updated usernames to file
			fileReader.SaveUsernames(args.Length > 2 ? args[2] : "");
		}

		public override void PrintHelp()
		{
			Console.WriteLine("Removes a username from the list. Argument can either be index in list or username contents");
			Console.WriteLine("  Example usage:");
			Console.WriteLine("    NamesReader remove Alice");
			Console.WriteLine("    NamesReader add Bob names.txt");
		}
	}
}